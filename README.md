# basic-math-teacher

Basic ruby program for chechink multiplication in primary kids

## How to use
git clone git@gitlab.com:montells/basic-math-teacher.git

`cd teacher`

`chmod +x teacher.rb`

add this line to your .bash_aliases
alias repasar='clear ; ruby ~[directory where you git clone]/teacher/teacher.rb'

So kid can come and type "repasar"

##Exit
type "x" or "exit"
