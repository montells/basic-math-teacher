#!/usr/bin/env ruby
$checked = nil
$show_products_table = true
$window_width = 115
$correct_answers = 0
$failed_answers = 0
$operation = 'm'

class String
  # colorization
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def green
    colorize(32)
  end

  def yellow
    colorize(33)
  end

  def blue
    colorize(34)
  end

  def pink
    colorize(35)
  end

  def light_blue
    colorize(36)
  end
end

module Operation
  class MathOperation

    attr_reader :first_operator, :second_operator, :result

    def initialize
      @first_operator = rand(10)
      @second_operator = rand(10)
      @text_test = set_text_test
    end

    def result
      @result ||= perform_operation
    end

    def show_test
      @text_test
    end

    def show_full_operation
      "#{display_test} = #{@result}"
    end

    private
    def perform_operation
      #override this method for concrete operation
      0
    end

    def set_text_test
      #override this method for concrete operation
      "a + b"
    end
  end

  class Multiplication < MathOperation

    private
    def perform_operation
      first_operator * second_operator
    end

    def set_text_test
      "#{@first_operator} * #{@second_operator}"
    end
  end

  class Sum < MathOperation

    private
    def perform_operation
      first_operator + second_operator
    end

    def set_text_test
      "#{@first_operator} + #{@second_operator}"
    end
  end

  class Rest < MathOperation

    def initialize
      @first_operator = rand(19) + 1
      @second_operator = rand(@first_operator)
      @text_test = set_text_test
    end

    private
    def perform_operation
      first_operator - second_operator
    end

    def set_text_test
      "#{@first_operator} - #{@second_operator}"
    end
  end

  class Division < MathOperation

    def initialize
      @second_operator = rand(9) + 1
      @first_operator =  @second_operator * rand(10)
      @text_test = set_text_test
    end

    private
    def perform_operation
      first_operator / second_operator
    end

    def set_text_test
      "#{@first_operator} : #{@second_operator}"
    end
  end
end

def set_operations_to_train
  operation = 'm'
  print 'Que operación deseas repasar Suma(s), Resta(r), Multiplicación(m) o División(d) o Cualquiera(c): '
  operation = gets.chomp!
  $operation = operation
end

def generate_operation
  valid_operations = %w(s r m d)
  operation_to_eval = ($operation == 'c') ? valid_operations[rand(3)] : $operation
  case operation_to_eval
  when 'm'
    Operation::Multiplication.new
  when 'd'
    Operation::Division.new
  when 's'
    Operation::Sum.new
  when 'r'
    Operation::Rest.new
  end
end

def complete_string_to(str, complete_to = 2, adjust_dir = :left)
  fragment_for_complete = ' '*(complete_to - str.length)
  return str + fragment_for_complete if adjust_dir == :left
  fragment_for_complete + str
end

def show_table
  if $show_products_table
    puts 'Tabla de productos'
    for first_factor in (1..9) do
      for second_factor in (2..9) do
        print complete_string_to "#{second_factor} * #{first_factor} = #{first_factor * second_factor}", 15
      end
      puts
    end
    puts "="*$window_width
  end
end

def show_evaluation
  if $checked == true
    puts "Respuesta correcta".green
  elsif $checked == false
    puts "Nooo, incorrecto. El resultado es #{$correct_answer}".red
  end
end

def evaluate(answer)
  $checked = Integer(answer) == $correct_answer
  if Integer(answer) == $correct_answer
    $correct_answers += 1
    $checked = true
  else
    $failed_answers += 1
    $checked = false
  end
end

def salutation
  print complete_string_to('Suerte en tus repasos Rocio. ', $window_width - 40)
  print complete_string_to("Correctas: #{$correct_answers}", 20, :right).green
  print complete_string_to("Incorrectas: #{$failed_answers}", 20, :right).red
  puts
end

set_operations_to_train
loop do
  show_table
  salutation
  show_evaluation
  operation = generate_operation
  print "Cuanto es #{operation.show_test} = "
  $correct_answer = operation.result
  respuesta = gets.chomp
  if respuesta == 'x' || respuesta == 'exit'
    puts "Hiciste #{$correct_answers} bien y #{$failed_answers} mal"
    break
  elsif respuesta == 'no se' || respuesta == 'nose'
    puts "estas embarcada. Es #{$correct_answer}"
  elsif respuesta == 'p'
    $show_products_table = !$show_products_table
  else
    evaluate(respuesta)
  end
  system "clear"
end
